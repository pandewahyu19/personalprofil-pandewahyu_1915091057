<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EducationController extends Controller
{
    public function education()
    {
        return view('Education',[
            "judul" => "education",
            "A"=> "SD NO 1 TISTA",
            "B"=> "SMP NEGERI 1 KERAMBITAN",
            "C"=> "SMA NEGERI 1 KERAMBITAN",
            "D"=> "KANIVA INTERNASIONAL",
            "E"=>"UNIVERSITAS PENDIDIKAN GANESHA",
          
        ]);
    }
}
