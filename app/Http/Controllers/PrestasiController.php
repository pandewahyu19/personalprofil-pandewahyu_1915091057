<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrestasiController extends Controller
{
    public function prestasi()
    {
        return view('prestasi',[
            "judul" => "prestasi",
            "img1" => "img/kidung.jpg",
            "img2" => "img/pentas.jpg",
            "img3" => "img/musik.jpg",
            "detail1"=> "Saya pernah mengikuti Lomba dharma gita atau bisa kita sebut kidung Bali pada saat masih di bangku SD dan juga SMP",
            "detail2"=> "Pernah ikut memeriahkan Pementasan beleganjur dan Gong Gede di festival Tista dan juga festtival Kerambitan",
            "detail3" => "Tahun lalu saya pernah mengikuti lomba musical puisi bersama teman Kuliah saya di UNIVERSITAS PENDIDIKAN GANESHA",
            
        ]);
    }
}
