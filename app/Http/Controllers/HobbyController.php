<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HobbyController extends Controller
{
    public function hobby()
    {
        return view('Hobby',[
            "judul" => "hobby",
           
            "image1"=>"img/sing.jpg",
            "link1" =>"https://www.google.com/url?sa=i&url=https%3A%2F%2Fcewekbanget.grid.id%2Fread%2F061612671%2Fsemua-orang-bisa-nyanyi-dengan-suara-merdu-dengan-4-teknik-mudah-ini%3Fpage%3Dall&psig=AOvVaw2R7-hzQvSZfK4RhSxgFuLW&ust=1632978166862000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCID484K0o_MCFQAAAAAdAAAAABBX ",
            "image2"=>"img/rindik.jpg",
            "link2" =>"https://www.google.com/url?sa=i&url=https%3A%2F%2Ffactsofindonesia.com%2Fbalinese-traditional-music-instrument&psig=AOvVaw2MQqBmmD0ZxzDdMdzfB8_h&ust=1632978553539000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCKDJjKq1o_MCFQAAAAAdAAAAABAJ",
            "image3"=>"img/gitar.jpg",
            "link3" =>"https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.wallpaperbetter.com%2Fid%2Fhd-wallpaper-abewc&psig=AOvVaw3YkmmKM8xkuBa3l6VLod96&ust=1632979087742000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCKjAjam3o_MCFQAAAAAdAAAAABAb",
            "image4"=>"img/game.jpg",
            "link4" =>"https://www.google.com/url?sa=i&url=https%3A%2F%2Fhai.grid.id%2Fread%2F07600933%2Ftips-ampuh-anti-lag-saat-main-mobile-legends%3Fpage%3Dall&psig=AOvVaw0Q8Lxq4axCdFuzOS9Fu6xh&ust=1632979407698000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCNi_qMW4o_MCFQAAAAAdAAAAABAD",
            "image5"=>"img/bola.jpg",
            "link5" =>"https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.plimbi.com%2Farticle%2F171316%2F5-kesalahan-yang-sering-dilakukan-saat-bermain-futsal&psig=AOvVaw2lbP8ndPFOn8kpFG-oB3TX&ust=1632979618930000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCIjTkaq5o_MCFQAAAAAdAAAAABAh",
            "image6"=>"img/libur.jpg",
            "link6" =>"https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.infodenpasar.com%2F2020%2F11%2F5-pantai-di-bali-yang-sepi-pengunjung.html&psig=AOvVaw1Bqjo8iWhOtZ_otdJ1Ny5Z&ust=1632979822398000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCIDao466o_MCFQAAAAAdAAAAABAx",
            "image7"=>"img/cook.jpg",
            "link7" =>"https://www.google.com/url?sa=i&url=https%3A%2F%2Ffood.detik.com%2Finfo-kuliner%2Fd-4482407%2Fagar-tak-kelamaan-di-dapur-saat-masak-lakukan-10-trik-praktis-ini&psig=AOvVaw3-yp3si6Or5htWx_jSybvD&ust=1632980101848000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCIDw6Y27o_MCFQAAAAAdAAAAABAD",
           
        ]);
    }
}
