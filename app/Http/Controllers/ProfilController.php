<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfilController extends Controller
{
    public function profil()
    {
        return view('profil',[
        
            "judul" => "profil",
            "nama" => "I Gede Pande Wahyu Semara Yoga",
            "email" => "bagus4744@gmail.com",
            "alamat" => "Provinsi Bali, Kabuupaten Tabanan, Kecamatan Kerambitan, Desa Tista",
            "jenis" => "Laki-Laki",
            "agama" => "Hindu",
            "deskripsi"=> "Perkenalkan saya Pande, saya adalah seorang mahasiswa Fakultas 
            Teknik Dan kejuruan, Jurusan Teknik Informatika, Prodi Sistem Informasi. Saya Kelahiran 9 mei 2000
            dan sekarang umur saya 21 tahun. saya adalah anak satu-satunya, dan saya tinggal bersama orang tua di rumah pribadi
            disebuah desa bernama desa Tista",
            
    ]);
    }
}
