@extends('beranda')
@section('container')

    
<section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Halaman Education</h2>
        
        </div>
        <h3 align="center">Education</h3>
        <div class="row">
          <div class="col-lg-4 col-md-6 icon-box">
            <div class="icon"><i class="bi bi-briefcase fa-3x"></i></div>
            <h4 class="title"><a href="">2007 - 20012</a></h4>
            <p class="description">{{ $A }}</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box">
            <div class="icon"><i class="bi bi-briefcase-fill fa-3x"></i></div>
            <h4 class="title"><a href="">2013 - 2015</a></h4>
            <p class="description">{{ $B }}</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box">
            <div class="icon"><i class="bi bi-briefcase fa-3x"></i></div>
            <h4 class="title"><a href="">2016 - 2018</a></h4>
            <p class="description">{{ $C }}</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box">
            <div class="icon"><i class="bi bi-briefcase-fill fa-3x"></i></div>
            <h4 class="title"><a href="">2018-2019</a></h4>
            <p class="description">{{ $D }}</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box">
            <div class="icon"><i class="bi bi-briefcase fa-3x"></i></div>
            <h4 class="title"><a href="">2019-NOW</a></h4>
            <p class="description">{{ $E }}</p>
          </div>
          

      </div>
    </section><!-- End Services Section -->

    
    
@endsection