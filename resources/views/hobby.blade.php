@extends('beranda')
@section('container')

<section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title">
        <h2> Halaman Hobby</h2>
        </div>
        <h4 align="center">By</h4>
      
        <h4 align="center"> Pandewahyu </h4><spam></spam><h3>Images :</h3>
   
        <section id="portfolio" class="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">Music</li>
              <li data-filter=".filter-card">E-sport</li>
              <li data-filter=".filter-web">Relax</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
            <img src="{{$image1}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Sing</h4>
                <div class="portfolio-links">
                <a href="{{$image1}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="{{$link1}}" title="More Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
            <img src="{{$image2}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Tradisional Instrumen</h4>
                <div class="portfolio-links">
                <a href="{{$image2}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="{{$link2}}" title="More Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
            <img src="{{$image3}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Modern instrument</h4>
                <div class="portfolio-links">
                <a href="{{$image3}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="{{$link3}}" title="More Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
            <img src="{{$image4}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Mobile Game</h4>
                <div class="portfolio-links">
                <a href="{{$image4}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="{{$link4}}" title="More Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
            <img src="{{$image5}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Foot Ball</h4>
                <div class="portfolio-links">
                <a href="{{$image5}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="{{$link5}}" title="More Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
            <img src="{{$image6}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Holiday</h4>
                <div class="portfolio-links">
                <a href="{{$image6}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="{{$link6}}" title="More Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
            <img src="{{$image7}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Cook</h4>
                <div class="portfolio-links">
                <a href="{{$image7}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="{{$link7}}" title="More Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Section -->

        
@endsection