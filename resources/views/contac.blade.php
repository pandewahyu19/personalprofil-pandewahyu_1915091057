@extends('beranda')
@section('container')


<section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2> Halaman Contact</h2>
          
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address ">
              <i class="bi bi-geo-alt fa-2x"></i>
              <h3>Address</h3>
              <p><a href="https://maps.app.goo.gl/dshV1Fk4FZeuN4My9">{{ $alamat }}</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="bi bi-phone fa-2x"></i>
              <h3>Phone Number</h3>
              <p><a href="http://wa.me/6285847209613">WA.{{ $telepon }}</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="bi bi-envelope fa-2x"></i>
              <h3>Email</h3>
              <p><a href="pande.wahyu@undiksha.ac.id">{{ $email }}</a></p>
            </div>
          </div>

          
        </div>

        <div class="form">
          <form action="forms/contact.php" method="post" role="form" class="php-email-form">
            <div class="row">
              <div class="col-md-6 form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              </div>
              <div class="col-md-6 form-group mt-3 mt-md-0">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email">
              </div>
            </div>
            <div class="form-group mt-3">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
            </div>
            <div class="form-group mt-3">
              <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
            </div>
         
            <div class="text-center"><button type="submit">Send Message</button></div>
          </form>
        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
            
          <div class="col-lg-3 col-md-6">
            <div class="footer-info">
              <h3>Personal Profil</h3>
              <p>
               {{$alamat}} <br>
                <br><br>
                <strong>Phone:</strong>{{ $telepon }}<br>
                <strong>Email:</strong> {{ $email }}<br>
              </p>
              <div class="social-links mt-3">
                <a href="https://t.tiktok.com/i18n/share/user/6839691417187533826/?_d=dd2a4gd251g445&language=id&sec_uid=MS4wLjABAAAASmxubwTfxvj-O0J0ZpwAXzr4s4OCNUowJz0SW6rAYO87L5easZWEWkwygxCcMH_p&share_author_id=6839691417187533826&u_code=dd2a4j51c1f2lc&timestamp=1632974182&user_id=6839691417187533826&sec_user_id=MS4wLjABAAAASmxubwTfxvj-O0J0ZpwAXzr4s4OCNUowJz0SW6rAYO87L5easZWEWkwygxCcMH_p&utm_source=whatsapp&utm_campaign=client_share&utm_medium=android&share_iid=7013397672431617818&share_link_id=3270081d-eea5-4e5e-bcf8-37b21aa471eb&share_app_id=1180" class="twitter"><i class="bx bxl-tiktok"></i></a>
                <a href="https://www.facebook.com/wahyu.semara.14" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="https://www.instagram.com/wahyusemara19/" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="https://twitter.com/PandeWahyuGami1?s=08" class="twitter"><i class="bx bxl-twitter"></i></a>
               
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>

          </div>
        </div>
      </div>
    </div>

@endsection