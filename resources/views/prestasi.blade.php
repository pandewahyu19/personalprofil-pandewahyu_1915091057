@extends('beranda')
@section('container')

<section id="why-us" class="why-us">
<div class="section-title">
          <h2> Halaman Pencapaian</h2>
          
        </div>
      <div class="container">
      <h3 align="center" >Achievment</h3>
        <div class="section-title">
     
        </div>
      
        <div class="row">
          <div class="col-lg-4">
            <div class="box">
              <h4>Lomba Musical puisi</h4>
              <p>{{$detail3}}</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box">
              <h4>Pementasan Festival</h4>
              <p>{{$detail2}}</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box" >
            
            <h4>Lomba Dharma Gita</h4>
            <p>{{ $detail1 }} </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Why Us S-->

@endsection