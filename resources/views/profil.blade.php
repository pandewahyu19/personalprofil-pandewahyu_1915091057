@extends('beranda')
@section('container')



<section id="about" class="about">
<div class="section-title">
          <h2> Halaman Profil</h2>
          
        </div>
        <div class="row no-gutters">
     
          <div class> </div>
          <img src="assets/img/pande.jpeg" alt="" class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start rounded-circle" >
          <div class="col-xl-7 ps-0 ps-lg-5 pe-lg-1 d-flex align-items-stretch">
            <div class="content d-flex flex-column justify-content-center">
            <div class="member-info">
                <div class="member-info-content">
                  <h4>Introdution</h4>
                  <span>Pealajar</span>
                  <div class="social">
                    <a href="https://twitter.com/PandeWahyuGami1?s=08"><i class="bi bi-twitter"></i></a>
                    <a href="https://www.facebook.com/wahyu.semara.14"><i class="bi bi-facebook"></i></a>
                    <a href="https://www.instagram.com/wahyusemara19/"><i class="bi bi-instagram"></i></a>
                  
                  </div>
                </div>
              <h3>Biodata</h3>
              <p>
              {{ $deskripsi }}
              </p>
              <div class="row">
                <div class="col-md-6 icon-box">
                  <i class="bi bi-person-circle fa-2x"></i>
                  <h4>Nama</h4>
                  <p>{{ $nama }}</p>
                </div>
                <div class="col-md-6 icon-box">
                  <i class="bi bi-geo-alt fa-2x"></i>
                  <h4>Alamat</h4>
                  <p>{{ $alamat }}</p>
                </div>
                <div class="col-md-6 icon-box">
                  <i class="bi bi-triangle-half fa-2x"></i>
                  <h4>Religion</h4>
                  <p>{{ $agama }}</p>
                </div>
                <div class="col-md-6 icon-box">
                  <i class="bi bi-gender-ambiguous fa-2x"></i>
                  <h4>Gender</h4>
                  <p>{{$jenis}}</p>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

   
@endsection
   
