<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="../../../fonts.googleapis.com/cssdda2.css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="../../../fonts.googleapis.com/cssab6d.css?family=Open+Sans:300,400,400i,600,600i,700" rel="stylesheet">

    <!-- Template CSS Files -->
 
    <link href="css/style.css" rel="stylesheet">

 
    <!-- Modernizr JS File -->
    <script src="js/modernizr.custom.js"></script>
    <title>personal profil | {{ $judul }} </title>
    
  </head>
  
  <body style="background: url(assets/img/one.jpg);">
 <!-- ======= Header ======= -->
  <header id="header"class="fixed-top d-flex align-items-center">
 <div class="container d-flex align-items-center justify-content-between">
      <nav id="navbar" class="navbar">
        <ul>
        <li><a class="nav-link  {{ ($judul === "beranda" )?'active':'' }} "  href="/">Beranda</a></li>
          <li><a class="nav-link {{ ($judul === "profil" )?'active':'' }} "  href="/profil">Profil</a></li>
          <li><a class="nav-link {{ ($judul === "education" )?'active':'' }} ""  href="/education">Education</a></li>
          <li><a class="nav-link {{ ($judul === "hobby" )?'active':'' }} "" href="/hobby">Hobby</a></li>
          <li><a class="nav-link {{ ($judul === "prestasi" )?'active':'' }} " " href="/prestasi">Pencapaian</a></li>
          <li><a class="nav-link {{ ($judul === "contac" )?'active':'' }} "" href="/contac">Contact</a></li>
         
          <li class="dropdown"> 
            <ul>
            <li>
              <li class="dropdown"><a href="#"><span>Menu</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="/">Beranda</a></li>
                  <li><a href="/profil">Profil</a></li>
                  <li><a href="/education">Education</a></li>
                  <li><a href="/hobby">Hobby</a></li>
                  <li><a href="/prestasi">Pencapaian</a></li>
                  <li><a href="/contac">Contac</a></li>
                </ul>
              </li>
              <li><a class="getstarted scrollto" href="#about">Get Started</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
            <form class="d-flex">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success " type="submit">Search</button>
      </form>
      </nav><!-- .navbar -->
</div>
</header>
    
<div class="container mt-4">
    @yield('container')
   </div>
   
   <section id="hero">
   <div class="hero-container">
      <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
  
<div class="carousel-item active" style="background: url(assets/img/slide/slide-1.jpg);">
  <div class="carousel-container">
    <div class="carousel-content">
      <h2 class="animate__animated animate__fadeInDown">PANDE WAHYU</h2>
      <p class="animate__animated animate__fadeInUp">Faculty of Egineering and Vocational, Department of Informatics Engineering</p>
      <div>
        <a href="/" class="btn-get-started animate__animated animate__fadeInUp scrollto">Back</a>
      </div>
  </div>
</div>
</div>
</div>
</div>
</div>
 <!-- ======= Footer ======= -->
 <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Undiksha</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/groovin-free-bootstrap-theme/ -->
        Designed by <a href="https://bootstrapmade.com/">Pande_wolf</a>
      </div>
    </div>
  </footer><
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

  
    

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="script.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
   <!-- Vendor JS Files -->
   <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

 
<script src="script.js"></script>
  </body>
  
</html>
