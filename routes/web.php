<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [App\Http\Controllers\BerandaController::class,'index']);

Route::get('/profil',[App\Http\Controllers\ProfilController::class,'profil'] );

Route::get('/education', [App\Http\Controllers\EducationController::class,'education'] );

Route::get('/hobby', [App\Http\Controllers\HobbyController::class,'hobby'] );

Route::get('/prestasi', [App\Http\Controllers\PrestasiController::class,'prestasi']);

Route::get('/contac', [App\Http\Controllers\ContacController::class,'contac']);


